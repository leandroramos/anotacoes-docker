# Minicurso Docker

## Aulas

- Aula 1 ([vídeo](https://drive.google.com/file/d/15Vze9QMYiw8P7TEZRgW3eW-GDyv8yKiD/view?usp=sharing)): introdução, `docker ps`, `docker run`, volumes
- Aula 2 ([vídeo](https://drive.google.com/file/d/1mJa5IENBXJkTRowBwfeqAcUiDBtTfEZr/view?usp=share_link)): Dockerfile, multistage build
- Aula 3 ([vídeo](https://drive.google.com/file/d/1YYiLP_rvEW-ucONHi6gJiBy3jNV0lQxA/view?usp=share_link)): docker compose
- Aula 4: Rede, lab

## Introdução

### O que são containers?

**Nota:** Container = isolamento.

Conainers são agrupamentos de aplicações com suas dependências. São semelhantes às máquinas virtuais, mas compartilham o Kernel do sistema operacional da máquina host.

Os containers podem ter somente o necessário para rodar uma aplicação, sem bibliotecas e programas desnecessários ao fim específico de rodar a aplicação.

### O que é o Docker?

Uma plataforma livre de _conteinerização_ de de aplicações, que permite o empacotamento das aplicações em containers.

### Relação entre imagens e containers
Uma imagem é um _snapshot_ de uma aplicação/sistema - um estado em que a aplicação/sistema se encontra em determinado momento. A imagem é imutável.

Um container depende de uma imagem para rodar, e implementa um ambiente de tempo de execução para a aplicação (runtime). Na camada dos containers podemos fazer alterações no ambiente, mas o container roda graças ao "template" fornecido pela imagem imutável.

![Relação entre imagens e containers](https://i.imgur.com/9MuaisG.png)

### Vantagens

- Uniformização de ambientes de desenvolvimento e produção
- Baixo custo computacional
- Facilidade de criar os ambientes e aplicações com scripts

## Aula 1

### Instalação do Docker no Debian

```
sudo apt remove -y docker docker-engine docker.io containerd runc
sudo apt update
sudo apt install -y ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  bullseye stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo adduser $USER docker
```

Para testar a instalação:
```
docker run hello-world
```

### Rodando comandos em um container

Bash do CentOS em um container rodando no Debian:
```
docker run -ti centos bash
```

Dentro do container:
```
cat /etc/os-release

uname -a
```
Podemos ver que, apesar de estarmos usando o bash no CentOS, o kernel é o da máquina host (Debian).
![Rodando o bash do CentOS no Debian](https://i.imgur.com/Shgj3UL.png)

### Docker CLI - comandos básicos

Listar containers em execução:
```
docker container ls # ou docker ps
```
Listar todos os containers, incluindo os que estão parados:
```
docker container ls -a # ou docker ps -a
```
Iniciar um container:
```
docker start nome_container
```
Parar um container:
```
docker stop nome_container
```
Listar as imagens disponíveis
```
docker image ls # ou docker images
```
Usar um terminal interativo no container:
```
docker exec -ti nome_container comando
```
Ver os logs de um container:
```
docker container logs -f nome_container
```

### Volumes

Volumes são diretórios da máquina host montados dentro do container, e permitem a persistência dos dados do container.

Os dados gerados durante a execução de um container são efêmeros, são descartados assim que um container deixa de existir.

Com volumes podemos guardar os dados e usá-los quando subirmos (ou criarmos outro com as mesmas especificações) o container novamente.

#### Criando um volume

Podemos criar um volume no diretório de trabalho:
```
docker run --name ubuntu-container --volume $(pwd)/teste:/teste -ti ubuntu bash
```
Entendendo a linha de comando acima:

- `docker run`: Pede para o Docker rodar um container
- `--name ubuntu-container`: Dá um nome ao container. Se não dermos um nome, o Docker dará um nome aleatório (e engraçado) a ele.
- `--volume $(pwd)/teste:/teste`:
    - `--volume`: Pedimos para que um volume seja criado
    - `$(pwd)/teste:/teste`:
        - diretorio_host:diretorio_container
        - `pwd` é o diretório onde estamos no momento. Então, $(pwd)/teste é _onde-estou/teste_
        - O que vem depois dos dois pontos (:) é o diretório que será montado no container - /teste (o mesmo que dizer _um diretório chamado teste na raiz do sistema_).
- `-ti`: O -t aloca um "pseudo-tty" e o -i mantém a entrada padrão aberta (interativo). Resumindo: -ti abre um terminal para interagirmos com o container.
- `ubuntu`: É o nome da imagem que vamos usar no container
- `bash`: É o comando que vamos executar no container. No caso, é o shell bash.

##### Exercício com volume

Vamos rodar um servidor nginx e usar o código HTML local no container:

```
# Criando o diretório de trabalho
mkdir -p nginx/html

# Entrando no diretório de trabalho
cd nginx

# Ajustando as permissões do volume
sudo chown -R $USER:www-data html/

# Criando o index.html que será servido pelo nginx
echo "<h1>Oi, Docker.</h1><p>Rodando dentro do container.</p>" > html/index.html

# Rodando o container nginx:
docker run \
    --name meu-nginx -d \
    -p 8080:80 \
    -v $(pwd)/html:/usr/share/nginx/html \
    nginx
```

![Rodando o container nginx com o volume html e abrindo redirecionando a porta 8080 do host para o 80 do container](https://i.imgur.com/hOvmq9j.png)

Podemos ver o resultado no navegador, em http://localhost:8080

![Página index.html sendo servida pelo nginx](https://i.imgur.com/et76bxf.png)

Se algo for alterado no diretório do volume, durante a execução do container, será preservado (persistido).

![Adicionando uma linha ao index.html no container](https://i.imgur.com/fkrNA3s.png)

![Linha adicionada à pagina](https://i.imgur.com/T5UzDUk.png)

Da mesma forma, podemos alterar o conteúdo do volume usando nossos editores de texto, na máquina host, e as mudanças serão servidas pelo nginx quando rodarmos o container - mesmo se o destruirmos e criarmos novamente.

Teste: remover e recriar o container:
```
docker rm --force meu-nginx

docker run \
    --name outro-nginx -d \
    -p 8080:80 \
    -v $(pwd)/html:/usr/share/nginx/html \
    nginx
```

![Editando o arquivo no meu editor de textos Gedit](https://i.imgur.com/05GBewl.png)

![Criando o novo container](https://i.imgur.com/7Qmdcek.png)

![Página alterada](https://i.imgur.com/Pc12U9j.png)

### Portainer CE

Portainer CE (Community Edition) é uma interface web para gerenciamento de containers.

```
# Criando o volume para os dados do Portainer
docker volume create portainer_data

# Rodando o container
docker run -d \
    -p 8000:8000 \
    -p 9443:9443 \
    --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:latest
```

No primeiro acesso, precisamos criar o nome de usuário e a senha.

![Criação de usuário administrativo no Portainer](https://i.imgur.com/qqL4NZj.png)

Página de "Quick Setup"

![Página de Quick Setup](https://i.imgur.com/pyZ68aO.png)

Ambiente local (com algumas configurações, podemos ter ambientes remotos também)

![Local environment](https://i.imgur.com/hSHw1NR.png)

Templates de Apps

![App templates](https://i.imgur.com/DoOjSWL.png)

Configurando um app Drupal pelo Portainer

![Configurando um app Drupal pelo Portainer](https://i.imgur.com/Y47JcgP.png)

Mapeamento de portas e volumes com o Portainer

![Mapeamento de portas e volumes com o Portainer](https://i.imgur.com/OjUeCcp.png)

Deploy do container meu-drupal

![Deploy do container Drupal](https://i.imgur.com/cbD51kv.png)

Lista de containers no ambiente local

![meu-drupal na lista de containers do Portainer](https://i.imgur.com/hxuswig.png)

Página de instalação do Drupal

![Página de instalação do Drupal](https://i.imgur.com/Udnix71.png)

Monitorando o uso de recursos do container meu-drupal

![Monitorando o uso de recursos do container meu-drupal](https://i.imgur.com/2117UzF.png)

Página inicial do novo site Drupal

![Página inicial do novo site Drupal](https://i.imgur.com/plDXlgC.png)

Pela imagem abaixo, podemos ver que seria melhor personalizar o volume.

![Informações sobre o volume do meu-drupal](https://i.imgur.com/LiFic8Q.png)

O volume permite editarmos os arquivos do Drupal

![Arquivos no volume](https://i.imgur.com/y5445Zf.png)

## Aula 2

### Criar e gerenciar imagens

#### Dockerfile

![Fluxo do Dockerfile](https://i.imgur.com/U8azl8F.png)

- FROM: Imagem na qual vamos nos basear
- RUN: Rodar comandos enquanto constrói o container (build)
- ENV: Variáveis de ambiente
- LABEL: Qualquer chave-valor
  - LABEL version="1.0.0"
  - LABEL description="Alguma descrição"
  - etc.
- VOLUME /var/www/html
  - O Docker vai criar um volume para essa pasta.
  - O volume será montado com nomes aleatórios, a menos que personalizemos na hora de rodar o container.
- EXPOSE 80
  - "Sr. Container, a porta 80 está em execução. Exponha, por favor."
  - `docker container run -P`
    - A opção -P verifica se tem expose e "binda" a porta do container em uma porta aleatória do host.

##### Exemplo de Dockerfile:

```
FROM debian

RUN apt-get update && apt-get install -y apache2 && apt-get clean

ENV APACHE_LOCK_DIR="/var/lock"
ENV APACHE_PID_FILE="/var/run/apache2.pid"
ENV APACHE_RUN_USER="www-data"
ENV APACHE_RUN_GROUP="www-data"
ENV APACHE_LOG_DIR="/var/log/apache2"

LABEL description="Webserver"

VOLUME /var/www/html/
EXPOSE 80

ENTRYPOINT ["/usr/sbin/apachectl"]
CMD ["-D", "FOREGROUND"]
```

Fazendo o build da imagem:

`docker image build -t meu_apache:1.0.0 .`

Rodando um container com a imagem recém-criada:

`docker container run -P meu_apache:1.0.0`

Para ver a porta que foi atribuída, use o `docker container ls` e veja a porta que foi redirecionada.

![Lista de containers em execução, com destaque para as portas do container escolhido](https://i.imgur.com/ebvVM94.png)

No caso acima, a porta 49153 do host foi direcionada para a porta 80 do container. Podemos escolher a porta através de parâmetros, também:

`docker container run --name meu_apache_container -p 8080:80 meu_apache:1.0.0`

![Lista de containers em execução, com destaque para as portas do container escolhido](https://i.imgur.com/v9PDeXp.png)

Podemos ver o volume que foi criado com o `docker container inspect meu_apache_container`:

No meu caso, com o RootDir do Docker alterado, o volume foi criado em /arquivos/docker/volumes/d4f1eb29073e138e06a1e53b1e53f7a52eb648ad4529f5e28ebc4370b8a04b3a/_data, e foi montado, no container, em /var/www/html.

![Mounts criados para o container](https://i.imgur.com/dhoVGYw.png)


##### CMD

**O CMD só passa parâmetros para o processo principal**

- O CMD serve para executar qualquer comando no container. 
  - `CMD /usr/sbin/apachectl -D FOREGROUND`
- Se existir um ENTRYPOINT, o CMD precisa passar o comando para o principal processo do container. E não para o Bash.
  - ENTRYPOINT ["/usr/sbin/apachectl"]
  - CMD ["-D", "FOREGROUND"]

##### ENTRYPOINT

É o principal processo do container.

##### Copiando arquivos para o container

Vamos usar o COPY no Dockerfile

```
FROM debian

RUN apt-get update && apt-get install -y apache2 && apt-get clean

ENV APACHE_LOCK_DIR="/var/lock"
ENV APACHE_PID_FILE="/var/run/apache2.pid"
ENV APACHE_RUN_USE="www-data"
ENV APACHE_RUN_GROUP="www-data"
ENV APACHE_LOG_DIR="/var/log/apache2"

COPY index.html /var/www/html

LABEL description="Webserver"
LABEL version="2.0.0"

VOLUME /var/www/html/
EXPOSE 80

ENTRYPOINT ["/usr/sbin/apachectl"]
CMD ["-D", "FOREGROUND"]
```

Vamos criar o arquivo index.html no mesmo diretório do Dockerfile:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HTML 5 Boilerplate</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Meu HTML5 Boilerplate</h1>
    <script src="index.js"></script>
  </body>
</html>
```

Vamos fazer o build da imagem e rodar o container:

```
docker image build -t meu_apache:2.0.0 .

docker container run -d -P meu_apache:2.0.0
```

###### ADD

O comando ADD copia arquivos e diretórios, com algumas diferenças em relação ao COPY:
- O ADD desempacota arquivos tar no container
- O ADD adiciona arquivos remotos também
- Segundo a [documentação de boas práticas na escrita de Dockerfile](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/), não é recomendável usar o ADD para baixar e descompactar arquivos remotos.
    - O COPY é mais transparente que o ADD (a "mágica" que ele faz não é muito clara).
    - É preferível usar combinações de `curl` ou `wget` com o `tar` para baixar e descompactar arquivos remotos. 



##### Mais opções no Dockerfile

- USER: Define o usuário padrão do container
- WORKDIR: Define o diretório padrão do container

```
FROM debian

RUN apt-get update && apt-get install -y wget apache2 && apt-get clean

ENV APACHE_LOCK_DIR="/var/lock"
ENV APACHE_PID_FILE="/var/run/apache2.pid"
ENV APACHE_RUN_USE="www-data"
ENV APACHE_RUN_GROUP="www-data"
ENV APACHE_LOG_DIR="/var/log/apache2"

WORKDIR /var/www/html/

RUN wget https://gist.githubusercontent.com/elmousa/3896469/raw/c7e6eb1c215b7203cc17c3e9a1ddeebab11ca549/index.html -O index.html

LABEL description="Webserver"
LABEL version="3.0.0"

# USER www-data (só como exemplo, para o container atual precisa ser o root)

VOLUME /var/www/html/
EXPOSE 80

ENTRYPOINT ["/usr/sbin/apachectl"]
CMD ["-D", "FOREGROUND"]
```

#### Dockerfile Multistage

##### O que é multistage build?

É um pipeline de construção de imagens Docker. Usando multistage podemos definir estágios diferentes para construir a imagem final, por exemplo:
- Um estágio para usar uma imagem de base e compilar um projeto, adicionando todas as dependências de construção, downloads de arquivos e outras etapas para que tenhamos o código compilado.
- Outro estágio que usa outra imagem de base e copia apenas o código compilado do estágio anterior, dispensando todas as camadas intermediárias que foram usadas para a compilação.

O resultado é uma imagem mais enxuta, menor e com menos camadas de construção, pois terá apenas o resultado da compilação do programa e o ambiente necessário para a sua execução.
 
Podemos fazer isso com vários Dockerfiles e, eventualmente, controlar a construção da imagem final usando um script em bash, por exemplo. Mas a construção em vários estágios, em um único Dockerfile, é mais simples de criar e manter.

##### Como o build multistage funciona?

- Cada FROM é um estágio da construção da imagem
  - Cada estágio pode copiar artefatos dos estágios anteriores
  - A cópia pode eliminar camadas intermediárias, como downloads, instalação de dependências, testes, etc. O próximo estágio fica apenas com o resultado da construção do estágio anterior.
- A imagem final é construída a partir do último estágio executado no Dockerfile.

##### Exemplo com React

O exemplo a seguir usa o projeto https://github.com/LukeMwila/builder-pattern-example.git

```
# Dockerfile

FROM node:12.13.0-alpine as build 
WORKDIR /app
COPY package*.json ./
RUN npm install 
COPY . .
RUN npm run build

FROM nginx 
EXPOSE 3000
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html
```

No exemplo acima, o primeiro FROM usa a imagem node:12.13.0-alpine e dá o nome ao estágio - _build_ - e faz a construção da aplicação usando o npm. O resultado disso inclui as etapas intermediárias, como a instalação de pacotes necessários para a construção da aplicação.

O segundo FROM usa a imagem do nginx, que será o servidor web da aplicação, e depois expõe a porta 3000, copia a configuração do nginx e, por fim, copia o **resultado** do primeiro estágio (`COPY --from=build`) para o diretório do nginx.

### Fazendo o build da imagem

```
docker image build -t meu-app-react:1.0 .
```

### Rodando um container com a nova imagem

```
docker container run --name meu_app_react -p 3000:3000 meu-app-react:1.0
```

## Compartilhando as imagens

- Docker Hub
- Arquivo
  `docker save myimage:latest | gzip > myimage_latest.tar.gz`

## Aula 3

### Docker Compose

Ferramenta para criar e compartilhar aplicações multi-container.
Com um arquivo YAML, podemos definir serviços, redes, volumes e tudo que a aplicação precisa para funcionar.
Arquivos para fazer deploy dos componentes da aplicação.

#### Fluxo de trabalho com o Docker Compose

1. Definir o ambiente da aplicação com Dockerfile
2. Definir os serviços no `docker-compose.yml` para que eles rodem em conjunto, em um ambiente isolado
3. Rodar o `docker compose up` para iniciar todos os serviços da aplicação

#### Estrutura do arquivo docker-compose.yml

- Versão do arquivo de compose
- Os serviços que serão construídos
- Volumes usados
- Redes que vão conectar os serviços

#### Exemplo de docker-compose.yml

```
version: '3.8'

services:
  db: 
    image: mariadb:latest
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress

volumes:
  db_data:
```

##### Subindo a aplicação com o Compose

Com o arquivo docker-compose.yml acima, podemos subir a aplicação:

- Remover eventuais containers anteriores: `docker compose rm`
- Subir a aplicação: `docker compose up`

A aplicação estará disponível em http://localhost:8000

![Wordpress rodando em localhost:8000](https://i.imgur.com/yZFF5tz.png)

![Wordpress instalado - tela do Admin Dashboard](https://i.imgur.com/BAzsicy.png)

##### Volumes da aplicação

Podemos ver que o volume com os dados do banco de dados foram montados no diretório de volumes padão do Docker (sem alterações, seria /var/lib/docker/volumes/wordpress_db_data/_data).

Para listar todos os volumes do Docker (não apenas os da aplicação WordPress):

`docker volume ls`

![Volume do banco de dados da aplicação](https://i.imgur.com/67wCg3e.png)

##### Inspecionando os containers

Vamos listar os containers com o comando `docker container ls`:

- wordpress-wordpress-1
- wordpress-db-1

Inspecionando os containers, podemos obter várias informações úteis. Destaco algumas:

`docker container inspect wordpress-db-1`

- Path: caminho do script de entrada do container.
    - No caso do container wordpress-db-1, o caminho vem do Dockerfile da imagem Docker MariaDB: `ENTRYPOINT ["docker-entrypoint.sh"]`
- Args: argumentos passados para o ENTRYPOINT.
    - No caso do container wordpress-db-1, o argumento está definido no Dockerfile da imagem Docker MariaDB: `CMD ["mysqld"]`. O Dockerfile (inalterado) da imagem MariaDB pode ser visto aqui: https://github.com/MariaDB/mariadb-docker/blob/master/Dockerfile.template
- State: várias informações sobre o status do container.
- HostConfig:
    - NetworkMode: a rede que os containers estão usando para o compartilhamento de recursos. Como não definimos uma rede, ela se chama `wordpress_default`.
    - RestartPolicy:
        - no (padrão)
            - O container não será reiniciado automaticamente.
        - on-failure[:max-retries]
            - O container será reiniciado em caso de falha (o número de tentativas de reinício é opcional).
        - always
            - O container sempre será reiniciado quando ele parar.
                - Se ele for parado manualmente, será reiniciado somente quando o serviço docker reiniciar ou quando o container for iniciado manualmente.
        - unless-stopped
            - Se parece com o always. Se o container for parado manualmente, ele não é iniciado nem quando o serviço docker reiniciar, necessitando seu início manual.
- Mounts:
    - Volumes montados no container.
- Config:
    - Várias informações sobre a configuração do container, incluindo variáveis de ambiente.
- NetworkSettings:
    - Várias informações sobre a rede do container. Muito útil se precisarmos de intervenções manuais entre os containers.

#### Conceitos gerais sobre Docker Compose

##### Services

_Services_ definem cada container que fará parte da composição da aplicação. Cada container rodará um serviço necessário ao funcionamento da aplicação.

##### Base Image

É a imagem que servirá de base para cada serviço. Podemos usar Dockerfile se quisermos personalizar as imagens e usar o _build_ para construir a imagem definitiva do serviço.

##### Ports

Mapeamento de portas entre os serviços e/ou entre host e container.

```
# Expondo portas entre os serviços linkados.
# As portas não serão mapeadas a portas do host.
expose:
  - "3000"
  - "8000"

# Mapeando portas do host para o container do serviço:
ports:
  - "8000:80"
```

##### Commands

Se comportam como o `CMD` do Dockerfile.

```
app:
  container_name: node_app
  restart: always
  build: ./
  ports:
    - '3000:3000'
  command:
    - 'npm run start'
```

##### Volumes

- Normal volumes
    - Especificamos um diretório do container que precisa ser persistido.
    - Docker Engine cria o volume.

```
volumes:
  - /var/lib/mysql
```

- Path mapping
    - Mapeamento de diretórios do host para o container.
    - Definimos o caminho de um diretório do host e fazemos com que ele seja montado no container.

```
volumes:
  - /opt/data:/var/lib/mysql
```

- Named volumes
    - Podemos criar nomes para os volumes. Isso facilita o uso de volumes entre containers e serviços.

```
  volumes:
    - meuvolume:/var/lib/mysql
  
# No fim do arquivo:
volumes:
  meuvolume:
```

##### Dependencies

Na aplicação usada neste tutorial, o WordPress precisa que o servidor MariaDB esteja pronto para uso. Por isso colocamos o serviço `db` como dependência para o funcionamento do serviço `wordpress`.



##### Environment Variables

Variáveis de ambiente que estarão disponíveis no container para diversas finalidades. Na aplicação de exemplo usada no tutorial, definimos as variáveis de ambiente para a conexão com o servidor MariaDB.

Variáveis de ambiente em uma lista:
```
web:
  environment:
    - NODE_ENV=production
```

Variáveis de ambiente em um arquivo:
```
web:
  env-file:
    - variaveis.env
```

Passando variáveis do shell do host para o container (isso vai passar o valor da variável no shell que está rodando o docker compose):
```
web:
  environment:
    - NODE_ENV
```

## Aula 4

### Networking

Por padrão, o Docker Compose cria uma rede para a aplicação, e os container podem se comunicar através da rede criada, usando os _hostnames_ dos containers para a comunicação.

Listando as redes do Docker:
`docker network ls`

![Saída do comando docker network ls](https://i.imgur.com/1jZtwVI.png)

Informações sobre a rede `bridge`:

![Saída do comando docker network inspect bridge](https://i.imgur.com/gbU903b.png)

Como nossa aplicação está em um diretório chamado `wordpress`, o Docker Compose cria uma rede com o nome do diretório como prefixo, seguido pelo nome `default`: `wordpress_default`.

Se quisermos mudar o prefixo, podemos sobrescrever o nome do projeto com a variável de ambiente `COMPOSE_PROJECT_NAME` (no host) ou com a _flag_ `--project-name` na linha de comando.

#### Links

Links podem ser usados se precisarmos de _aliases_ para acessar outros serviços da aplicação. Por padrão, os _hostnames_ dos containeres são os mesmos nomes dos serviços. No exemplo abaixo, podemos, a partir do serviço _web_, acessar o banco de dados usando os _hostnames_ `db` ou `database`.

```
version: "3.9"
services:
  web:
    build: .
    links:
      - "db:database"
  db:
    image: postgres
```

#### Redes pré-existentes

Se precisarmos que os containers ingressem em uma rede já existente, podemos usar a opção _external_ na configuração de rede _default_.

```
services:
  # ...
networks:
  default:
    name: minha-rede-pre-existente
    external: true
```

##### Demonstração de duas aplicações usando a mesma rede

`docker network create minha-rede-pre-existente`

`git clone https://github.com/bbachi/react-nodejs-docker-compose.git`

Adicionar a rede pré-existente ao docker-compose.yml e rodar com `docker compose up`.

#### Multi-host networking

Podemos usar o driver nativo de rede _overlay_ para criar redes entre vários hosts, criando clusters no _modo swarm_ do Docker (fora do escopo deste tutorial).

#### Custom networks

Se a rede padrão não for suficiente, podemos criar novas redes. Por exemplo: podemos definir uma aplicação web que tenha duas redes, uma _frontend_ e uma _backend_. O serviço de proxy acessa somente a rede _frontend_, o serviço de banco de dados acessa somente a rede _backend_, e o serviço app acessa as duas redes. Não trataremos disso neste turorial, mas segue um exemplo de Docker Compose usando o recurso:

```
version: "3.9"

services:
  proxy:
    build: ./proxy
    networks:
      - frontend
  app:
    build: ./app
    networks:
      - frontend
      - backend
  db:
    image: postgres
    networks:
      - backend

networks:
  frontend:
    # Use a custom driver
    driver: custom-driver-1
  backend:
    # Use a custom driver which takes special options
    driver: custom-driver-2
    driver_opts:
      foo: "1"
      bar: "2"
```

## Tópicos sugeridos

- Segurança: não expor dados da máquina local
- Volumes e exports
- Permissões dentro do container
- Permissões fora do container
- Herança de arquivos de configuração do Docker Compose.
  - dev
  - build
  - prod
  - etc.
